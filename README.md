# clickable-gitlab-ci-test

An example Ubuntu Touch app for using GitLab's CI to build & publish.

Original posted on
[my blog](http://blog.bhdouglass.com/clickable/ubuntu-touch/2018/07/05/clickable-and-continuous-integration.html).

## Overview

The `.gitlab-ci.yml` file defines the build steps for use with GitLab's CI.
There are two steps defined in this file, `publish` and `review`. `publish`
will only run when a git tag is pushed to GitLab (as defined by `only: - tags`).
On the other hand, the `review` step will run only anything but a git tag (because
it uses `except: - tags`). Both steps define a `script` which can be multiple
lines of shell script, but for the purposes of the demo is just a single
line calling various commands of clickable. After the script is complete GitLab
will collect `artifacts` based on the defined `path` (in this case `build/*.click`).
This click file will be made available for download via Gitlab, but only for
one week because `expire_in: 1 week`.

## License

Copyright (C) 2022 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
